#include <git2.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "common.h"

static int use_remote(git_repository *repo, char *name)
{
	git_remote *remote = NULL;
	int error;
	const git_remote_head **refs;
	size_t refs_len, i;
	git_remote_callbacks callbacks = GIT_REMOTE_CALLBACKS_INIT;
	
	printf ("0 %p %s\n", remote, name);
	// Find the remote by name
	error = git_remote_lookup(&remote, repo, name);
	printf ("1 %p %s\n", remote, name);
	if (error < 0) {
		error = git_remote_create_anonymous(&remote, repo, name);
		if (error < 0)
			goto cleanup;
	}

	/**
	 * Connect to the remote and call the printing function for
	 * each of the remote references.
	 */
	git_cred ** out; 
	char * pk = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDY/hVRSw8VlRzf9MYraLFiemPWLmpGJSwYXM3c6Y44cjo2+0jK59QbXGOM03kht1JPUOex26TFRwMItYf6Q3BCApbB7LVVfdl+c/9ounWS+z0StavqRFOhQWJIaGc4YC6YfIVZyzu84mv6ED9CW011dLiG0AnaM0Z0vgBKg0+Dg9IehXvWyQ8x3+SVT1NIl+2K40ME856/EL/SRVs9QGAa4uTuLa6hlp2DnNWYoMQ3YUYVBlzo/Qna3VU74+Y0DbNZZLH+5KmTOiov1/AD7K37i5BmIte/pnGxBfLSpa8Pu2kvIONepIIicu0HAuCR+tYUXYyUIqKxWPdQQixfjegD audris@utk.edu";
	int pkl = strlen (pk);
	//int ok = git_cred_ssh_custom_new (git_cred **out, "git", pk, pkl, git_cred_sign_callback sign_callback, void *payload);

	callbacks.credentials = cred_acquire_cb;

	printf ("2 %p %s\n", remote, name);
	error = git_remote_connect(remote, GIT_DIRECTION_FETCH, &callbacks, NULL);
	if (error < 0)
		goto cleanup;

	/**
	 * Get the list of references on the remote and print out
	 * their name next to what they point to.
	 */
	printf ("3 %p %s\n", remote, name);
	if (git_remote_ls(&refs, &refs_len, remote) < 0)
		goto cleanup;

	printf ("4 %p %p %lu %s\n", remote, refs, refs_len, name);
	for (i = 0; i < refs_len; i++) {
		char oid[GIT_OID_HEXSZ + 1] = {0};
		git_oid_fmt(oid, &refs[i]->oid);
		printf("%s\t%s\n", oid, refs[i]->name);
	}

cleanup:
	git_remote_free(remote);
	return error;
}

/** Entry point for this command */
int ls_remote(git_repository *repo, int argc, char **argv)
{
	int error;

	if (argc < 2) {
		fprintf(stderr, "usage: %s ls-remote <remote>\n", argv[-1]);
		return EXIT_FAILURE;
	}

	error = use_remote(repo, argv[1]);

	return error;
}
