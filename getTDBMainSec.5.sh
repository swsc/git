#!/bin/bash
LD_LIBRARY_PATH=$HOME/lib:$HOME/lib64:$LD_LIBRARY_PATH

k=${2:-1}
cloneDir=${3:-/lustre/medusa/audris/$k}
base=New20151010$k
i=$(echo $1-1|bc)


tt=$TMPDIR
cd $tt
echo START $(date +"%s") on $(hostname):$(pwd) 
this=$(echo $tt | sed 's|^/tmp/||')
ls -l .. | grep audris | awk '{print $NF}' | grep -v $this  | while read dd
do echo removing /tmp/$dd 
   find /tmp/$dd -delete
done

free=$(df -k .|tail -1 | awk '{print int($3/1000000)}')
#echo $(df -k . |tail -1)
echo "START df=$free " $(date +"%s") 
#exit
if [[ "$free" -lt 600 ]] 
then echo "not enough disk space $free, need 600+GB"
     exit
fi


m=$i
gunzip -c $cloneDir/${base}.list.$m.gz | cut -d\; -f1 | uniq > CopyList.$k
cat CopyList.$k | while read d
do echo "cp -pr $cloneDir/$d $TMPDIR" at $(date +"%s") 
   cp -pr $cloneDir/$d .  
   echo "done cp" at $(date +"%s") 
done
free=$(df -k .|tail -1 | awk '{print $3/1e6}')
echo "COPIED df=$free " $(date +"%s")

nlines=$(gunzip -c $cloneDir/${base}.list.$m.gz|wc -l)
part=$(echo "$nlines/16 + 1"|bc)

rm -f pids.$m
echo starting "$m gunzip -c $cloneDir/$base.list.$m.gz | split -l $part --numeric-suffixes - $base.list.$m. and $HOME/bin/cln.sec.sh $cloneDir $base $m $l"
gunzip -c $cloneDir/$base.list.$m.gz | split -l $part --numeric-suffixes - $base.list.$m.
for l in 00 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15
do ($HOME/bin/cln.sec.sh $cloneDir $base $m $l; cp -p "${base}.$m.$l"*.err "${base}.$m.$l"*.idx "${base}.$m.$l"*.bin $cloneDir/) &
   pid=$!
   echo "pid for $m.$l is $pid" $(date +"%s")
   echo "$pid" >> pids.$m
done


cat pids.$m | while read pid
do echo "waiting  for $pid" $(date +"%s") 
  while [ -e /proc/$pid ]; do sleep 30; done
  free=$(df -k .|tail -1 | awk '{print $3/1e6}')
  echo "after waiting for $pid df=$free " $(date +"%s")
done
free=$(df -k .|tail -1 | awk '{print $3/1e6}')
echo "after waiting df=$free "  $(date +"%s")

cat CopyList.$k | while read d
do find $d -delete
done
rm CopyList.$k 

free=$(df -k .|tail -1 | awk '{print $3/1e6}')

echo "after rm df=$free "  $(date +"%s")

echo DONE $(date +"%s")



