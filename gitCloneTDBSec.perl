use strict;
use warnings;
use Compress::LZF ();
use Digest::SHA1 qw (sha1_hex);


use line;
#########################
# create code to versions database
#########################
use TokyoCabinet;

my $fbase = $ARGV[0];
my $parts = 1;
$parts = $ARGV[1] if defined $ARGV[1];

my (%fhb, %fhi, %size);
for my $j (0 .. ($parts-1)){
	open $fhi{$j}, '>', "${fbase}_$j.idx";
	open $fhb{$j}, '>', "${fbase}_$j.bin";
	$size{$j} = 0;
}
	 
my %cmd;	
my $dir0 = "";

while(<STDIN>){
	chop();
	my ($dir, $f) = split(/\;/, $_, -1);
	if ($dir0 eq ""){
		$dir0 = $dir;
	}else{
		if ($dir0 ne $dir){
			output($dir0);
			$dir0 = $dir;
			%cmd = ();
		}
	}
	next if $f =~ /\=\>/;#indicates copies/renames:ccbd73a450bd33362373a3a066beffb348f528a7:lib/Test/Simple/t/{ => Builder}/output.t
	my @ff = split(/\//, $f, -1);
	my $vs = pop @ff;
	$f = "/".(join '/', @ff);
	if ($f ne "" && $f !~ / => /){
		# Now checking for binary in git
		#next if !($f =~ /$matchExt/);
		$cmd{$vs}{$f}++;
	}
}	

output($dir0);

sub output {
	my $dir =  $_[0];
	my $tmpFile = "stuff.$dir.$$";
	#print STDERR "running cat $tmpFile | $ENV{HOME}/bin/grabf ./$dir\n";
   open A, "> $tmpFile";
   for my $vs (keys %cmd){
	for my $f (keys %{$cmd{$vs}}){
      	print A "$vs\;$f/$vs\n";
      }
   }
	close A;
   open A, "cat $tmpFile | $ENV{HOME}/bin/grabf ./$dir|";
   my $state = 0;
   my ($rem, $line) = ("", "");
   while (<A>){
      if ($state == 0){
         $rem = $_;
         $state = 1;
         #print STDERR "Starting $_";
         next;
      }       
      if ($state == 1){
         if ($rem eq $_){
            #print STDERR "Finished $_";
            $state = 0;
            chop ($rem);
            my ($sha1, $f) = split(/\;/, $rem, -1);
            chop($line);
            dump_file ($line, "$dir$f",$sha1);
            $line = "";
            next;
         }else{
            $line .= $_;
         }
      }
   }
   close A;
	#print STDERR "Done\n";
	unlink $tmpFile;
}

sub dump_file {
	my ($code, $fv, $sha1) = @_;
	my $len = length($code);
	return if $len == 0;
	my $shaFull = sha1_hex ("blob $len\0$code");
	if ($sha1 ne $shaFull){ die "sha do not match: $fv\n"; }
	my $sec = hex (substr($shaFull,0,2)) % 128;

	my $codeC = safeComp ($code);
	my $lenC = length($codeC);
	my $sha1C = sha1_hex($codeC);

	my $j = 0;
	$j = $sec%$parts if ($parts > 1);
	my $fb = $fhb{$j};
	my $fi = $fhi{$j};
	print $fi "$size{$j};$lenC;$sec;$shaFull;$sha1C;$fv\n";
	print $fb $codeC;
	$size{$j} += $lenC;
}

sub safeDecomp {
	my ($codeC, @rest) = @_;
	try {
		my $code = Compress::LZF::decompress ($codeC);
		return $code;
	} catch Error with {
		my $ex = shift;
		print STDERR "Error: $ex, for parameters @rest\n";
		return "";
	}
}

sub safeComp {
	my ($code, @rest) = @_;
	try {
		my $codeC = Compress::LZF::compress ($code);
		return $codeC;
	} catch Error with {
		my $ex = shift;
		print STDERR "Error: $ex, for parameters @rest\n";
		return "";
	}
}


