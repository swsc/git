CC=gcc

# Enable all compiler warnings. 
CCFLAGS+= -I ~/include/

# Linker flags
LDFLAGS+=-L ~/lib -lgit2

LIBFLAGS=-L ~/lib -lgit2

SOURCES=$(wildcard *.c) 
OBJECTS=$(SOURCES:.c=.o)

all: classify grabc grabft grabf grabtag

classify: classify.o common.o 

grabc: grabc.o common.o 

grabft: grabft.o common.o 

grabf: grabf.o common.o 

grabtag: grabtag.o common.o 

%.o: %.c
	$(CC) $(CCFLAGS) -o $@ -c $<

clean:
	rm -rf *.o 

distclean: clean

	

