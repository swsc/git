/* Convert a SHA to an OID */

/* const char *sha = "4a202b346bb0fb0db7eff3cffeb3c70babbd2045";
git_oid oid = 0;
int error = git_oid_fromstr(&oid, sha);
git_blob *blob;
error = git_blob_lookup(&blob, repo, &oid);
git_blob *blob = NULL;
int error = git_blob_lookup(&blob, repo, &oid);

git_off_t rawsize = git_blob_rawsize(blob);
const void *rawcontent = git_blob_rawcontent(blob);
*/


/*
 * libgit2 "cat-file" example - shows how to print data from the ODB
 *
 * Written by the libgit2 contributors
 *
 * To the extent possible under law, the author(s) have dedicated all copyright
 * and related and neighboring rights to this software to the public domain
 * worldwide. This software is distributed without any warranty.
 *
 * You should have received a copy of the CC0 Public Domain Dedication along
 * with this software. If not, see
 * <http://creativecommons.org/publicdomain/zero/1.0/>.
 */

#include <git2.h>
#include <stdio.h>
#include "common.h"

static void print_signature(const char *header, const git_signature *sig)
{
  char sign;
  int offset, hours, minutes;

  if (!sig)
    return;

  offset = sig->when.offset;
  if (offset < 0) {
    sign = '-';
    offset = -offset;
  } else {
    sign = '+';
  }

  hours   = offset / 60;
  minutes = offset % 60;

  printf("%s %s <%s> %ld %c%02d%02d\n",
       header, sig->name, sig->email, (long)sig->when.time,
       sign, hours, minutes);
}

// Printing out a blob is simple, get the contents and print
	

static void show_blob(const git_blob *blob)
{
  /* ? Does this need crlf filtering? */
  fwrite(git_blob_rawcontent(blob), (size_t)git_blob_rawsize(blob), 1, stdout);
}

// Show each entry with its type, id and attributes
static void show_tree(const git_tree *tree)
{
  size_t i, max_i = (int)git_tree_entrycount(tree);
  char oidstr[GIT_OID_HEXSZ + 1];
  const git_tree_entry *te;

  for (i = 0; i < max_i; ++i) {
    te = git_tree_entry_byindex(tree, i);

    git_oid_tostr(oidstr, sizeof(oidstr), git_tree_entry_id(te));

    printf("%06o %s %s\t%s\n",
      git_tree_entry_filemode(te),
      git_object_type2string(git_tree_entry_type(te)),
      oidstr, git_tree_entry_name(te));
  }
}


// Commits and tags have a few interesting fields in their header.
static void show_commit(git_commit *commit)
{
  unsigned int i, max_i;
  char oidstr[GIT_OID_HEXSZ + 1];

  git_oid_tostr(oidstr, sizeof(oidstr), git_commit_tree_id(commit));
  printf("tree %s\n", oidstr);

  max_i = (unsigned int)git_commit_parentcount(commit);
  for (i = 0; i < max_i; ++i) {
    git_oid_tostr(oidstr, sizeof(oidstr), git_commit_parent_id(commit, i));
    printf("parent %s\n", oidstr);
  }

  print_signature("author", git_commit_author(commit));
  print_signature("committer", git_commit_committer(commit));


  printf("\ntime: %ld\n", git_commit_time(commit));
  
  if (git_commit_message(commit))
    printf("\nMessage: %s\n", git_commit_message(commit));

  printf("\nBody: %s\n", git_commit_body(commit));
}

static void show_tag(const git_tag *tag)
{
  char oidstr[GIT_OID_HEXSZ + 1];

  git_oid_tostr(oidstr, sizeof(oidstr), git_tag_target_id(tag));;
  printf("object %s\n", oidstr);
  printf("type %s\n", git_object_type2string(git_tag_target_type(tag)));
  printf("tag %s\n", git_tag_name(tag));
  print_signature("tagger", git_tag_tagger(tag));

  if (git_tag_message(tag))
    printf("\n%s\n", git_tag_message(tag));
}

enum {
  SHOW_TYPE = 1,
  SHOW_SIZE = 2,
  SHOW_NONE = 3,
  SHOW_PRETTY = 4
};

/* Forward declarations for option-parsing helper */
struct opts {
  const char *dir;
  const char *rev;
  int action;
  int verbose;
};

static void dump_tree(const git_tree *tree, char * pre, git_repository *repo, char * hash)
{
  size_t i, max_i = (int)git_tree_entrycount(tree);
  char oidstr[GIT_OID_HEXSZ + 1];
  const git_tree_entry *te;

  for (i = 0; i < max_i; ++i) {
    te = git_tree_entry_byindex(tree, i);
    const char * name = git_tree_entry_name(te);

    if (git_tree_entry_type(te) == GIT_OBJ_BLOB) {
       git_oid_tostr(oidstr, sizeof(oidstr), git_tree_entry_id(te));
       printf ("%s/%s;%s;%s\n", pre, name, oidstr, hash);
       git_object * objt;
       git_tree_entry_to_object (&objt, repo, te);
       if (git_object_type (objt) == GIT_OBJ_BLOB){
          git_blob * gb = (git_blob*) objt;
          fwrite(git_blob_rawcontent(gb), (size_t)git_blob_rawsize(gb), 1, stdout);
       }
       git_object_free (objt);       
       printf ("%s/%s;%s;%s\n", pre, name, oidstr, hash);
    } else {
       git_object * o1;
       git_tree_entry_to_object (&o1, repo, te);
       git_tree * t1 = (git_tree *) o1;
       char *result = malloc(strlen(pre)+strlen(name)+2);
       strcpy (result, pre);
       strcat (result, "/");       
       strcat (result, name);
       dump_tree (t1, result, repo, hash);
       free (result);
    }
  }
    
}



// Entry point for this command
int main(int argc, char *argv[])
{
   git_repository *repo;
   char oidstr[GIT_OID_HEXSZ + 1];

   git_libgit2_init();
   check_lg2 (git_repository_open_bare(&repo, argv[1]),
              "Could not open repository", NULL);
   char *l1 = NULL;
   size_t size = 0;
   while (getline(&l1, &size, stdin)>=0){
      git_object *obj = NULL;
      l1[strlen(l1)-1] = 0;      
      check_lg2(git_revparse_single(&obj, repo, l1),
                "Could not resolve", l1);

      git_tree * tree;
      strncpy (oidstr, l1, GIT_OID_HEXSZ);
      if (git_object_type(obj) == GIT_OBJ_COMMIT) {
         git_commit *commit = (git_commit*)obj;
         git_commit_tree (&tree, commit);
         git_oid_tostr(oidstr, GIT_OID_HEXSZ, git_tree_id(tree));
      } else {
         if (git_object_type (obj) == GIT_OBJ_TREE) {
            git_tree *tree = (git_tree*)obj;
         } else {
            fprintf(stderr, "wrong object type: could be commit or tree\n");
            continue;
         }
      }
      dump_tree (tree, "", repo, oidstr);
      git_object_free(obj);
   }
   free (l1);
   git_repository_free(repo);
   git_libgit2_shutdown();

  return 0;
}



