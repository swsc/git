#include <git2.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <glib.h>
#include "common.h"


GHashTable * hDone;
char * str = "1";


static void dump_tree_f (const git_tree *tree, char * pre, git_repository *repo, char * f)
{
  size_t i, max_i = (int) git_tree_entrycount(tree);

  for (i = 0; i < max_i; ++i) {
    const git_tree_entry * te = git_tree_entry_byindex(tree, i);
    const char * name = git_tree_entry_name (te);
    
    char oidstr[GIT_OID_HEXSZ + 1];
    git_oid_tostr (oidstr, sizeof(oidstr), git_tree_entry_id (te));
    
    char buf [10000];
    sprintf(buf, "%s/%s", pre, name);
    int l0 = strlen (buf);
    
    if (git_tree_entry_type(te) == GIT_OBJ_BLOB) {
      git_object * objt;
      gchar * bidstr = g_new (gchar, GIT_OID_HEXSZ + 1);
      git_oid_tostr (bidstr, GIT_OID_HEXSZ + 1, git_tree_entry_id (te));
      gpointer p = g_hash_table_lookup (hDone, bidstr);
      if (p == NULL){
	git_tree_entry_to_object (&objt, repo, te);
	git_blob * gb = (git_blob*) objt;
	int isBin = git_blob_is_binary (gb);
	//fprintf (stderr, "%s;%s;%d\n", oidstr, f, isBin);
	if (!isBin){
	  printf ("%s;%s;%s\n", oidstr, buf+1, f);
	  fwrite(git_blob_rawcontent(gb), (size_t)git_blob_rawsize(gb), 1, stdout);
	  printf ("\n%s;%s;%s\n", oidstr, buf+1, f);
	}else{
	  //printf ("bin:%s;%s;%s\n", oidstr, buf+1, f);
	}
	git_object_free (objt);
	g_hash_table_insert (hDone, bidstr, str);
      }else{
	g_free (bidstr);
      }
    } else {
      if (git_tree_entry_type(te) == GIT_OBJ_TREE){
	git_object * o1;
	git_tree_entry_to_object (&o1, repo, te);
	git_tree * t1 = (git_tree *) o1;
	char *result = malloc(strlen(pre)+strlen(name)+2);
	strcpy (result, pre);
	strcat (result, "/");       
	strcat (result, name);
	dump_tree_f (t1, result, repo, f);
	free (result);
	git_object_free (o1);
      } else {       
	fprintf (stderr, "unknown type %d for %s in %s/%s %s\n", git_tree_entry_type(te), oidstr, pre, name, f);
      }
    }
  }
}


// Entry point for this command
int main(int argc, char *argv[])
{
  git_repository *repo;
  git_object *obj = NULL;

  git_libgit2_init();
  if (check_lg2 (git_repository_open_bare(&repo, argv[1]),
		 "Could not open repository", NULL) != 0)
    exit (-1);

  char *l0 = NULL;
  size_t size = 0;
  hDone =  g_hash_table_new (g_str_hash, g_str_equal);
  
  while (getline(&l0, &size, stdin)>=0){
    char* l1 = strdup (l0);
    l1 [strlen(l1)-1] = 0;
    git_tree * tree;
    if (obj != NULL) git_object_free(obj);
    if (check_lg2(git_revparse_single(&obj, repo, l1),
		    "Could not resolve", l1) != 0) continue;
    if (git_object_type (obj) == GIT_OBJ_COMMIT) {
      git_commit *commit = (git_commit*)obj;
      git_commit_tree (&tree, commit);
    } else{
      fprintf(stderr, "not a commit\n");
      continue;
    }
    dump_tree_f (tree, "", repo, l1);
    free (l1);
  }
  git_repository_free (repo);
  git_libgit2_shutdown ();

  return 0;
}
