use strict;
use warnings;
use Compress::LZF ();
use Digest::SHA qw (sha1_hex sha1);


#########################
# create code to versions database
#########################
use TokyoCabinet;

sub fromHex { 
	return pack "H*", $_[0]; 
} 

my $sections = 128;
my $parts = 1;
$parts = $ARGV[1] if defined $ARGV[1];

#print STDERR "started:".(time())."\n"; 
my (%fhos);
for my $sec (0 .. ($sections-1)){
	my $pre = "/fast";
	$pre = "/fast" if $sec % $parts;
	tie %{$fhos{$sec}}, "TokyoCabinet::HDB", "$pre/All.sha1/shac_$sec.tch", TokyoCabinet::HDB::OREADER, 
     16777213, -1, -1, TokyoCabinet::TDB::TLARGE, 100000
		or die "cant open $pre/All.sha1/shac_$sec.tch\n";
}
#print STDERR "read hash:".(time())."\n"; 


my $fbase = $ARGV[0];

my (%fhb, %fhi, %size);
for my $j (0 .. ($parts-1)){
	open $fhi{$j}, '>', "${fbase}_$j.idx"  or die ($!);
	open $fhb{$j}, '>', "${fbase}_$j.bin"  or die ($!);
	$size{$j} = 0;
}
open FV, '>', "${fbase}.vs" or die ($!);
	 
my %cmd;	
my $dir0 = "";
while(<STDIN>){
	chop();
	my $dir = $_;
	open A, "git --git-dir=$dir log --pretty='%H' --all | $ENV{HOME}/bin/grabc $dir |";
   my $state = 0;
   my ($rem, $line) = ("", "");
   while (<A>){
      if ($state == 0){
			$rem = $_;
         $state = 1;
      } else {      
      	if ($state == 1){
         	if ($rem eq "$_"){
            	$state = 0;
            	chop ($rem);
            	my ($hsha1, $tr, $p, $t) = split(/\;/, $rem, -1);
					my $sec = hex (substr($hsha1,0,2)) % $sections;
					if (length ($line) == 0){
						#print STDERR "Empty:$hsha1;$dir/$f/$cmt\n";
						next;
					}
            	my $res = dump_file ($line, $hsha1, $sec, "$dir;$tr;$p;$t");
					if ($res ne "new"){
						print FV "$res;".(length ($line)).";$hsha1;$dir;$tr;$p;$t\n";
					}
            	$line = "";
         	}else{
            	$line .= $_;
         	}
			}
      }
   }
   close A;
}

sub dump_file {
	my ($code, $hsha1, $sec, $dir) = @_;
	my $len = length($code);
	return if $len == 0;

	my $sha1 = fromHex ($hsha1);
	if (defined $fhos{$sec}{$sha1}){
		return unpack 'w', $fhos{$sec}{$sha1};
	}

	my $hshaFull = sha1_hex ("commit $len\0$code");

	if ($hsha1 ne $hshaFull){ print STDERR "sha do not match: $dir: $hsha1 vs $hshaFull, $len\n$code"; }

	my $codeC = safeComp ($code);
	my $lenC = length($codeC);
	#my $hsha1C = sha1_hex($codeC);

	my $j = 0;
	$j = $sec%$parts if ($parts > 1);
	my $fb = $fhb{$j};
	my $fi = $fhi{$j};
	print $fi "$size{$j};$lenC;$sec;$hsha1;$dir\n";
	print $fb $codeC;
	$size{$j} += $lenC;
	return "new";
}

sub safeDecomp {
	my ($codeC, @rest) = @_;
	try {
		my $code = Compress::LZF::decompress ($codeC);
		return $code;
	} catch Error with {
		my $ex = shift;
		print STDERR "Error: $ex, for parameters @rest\n";
		return "";
	}
}

sub safeComp {
	my ($code, @rest) = @_;
	try {
		my $codeC = Compress::LZF::compress ($code);
		return $codeC;
	} catch Error with {
		my $ex = shift;
		print STDERR "Error: $ex, for parameters @rest\n";
		return "";
	}
}


