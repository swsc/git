#include <git2.h>
#include <stdio.h>
#include "common.h"

static void print_signature(const char *header, const git_signature *sig)
{
  char sign;
  int offset, hours, minutes;

  if (!sig)
    return;

  offset = sig->when.offset;
  if (offset < 0) {
    sign = '-';
    offset = -offset;
  } else {
    sign = '+';
  }

  hours   = offset / 60;
  minutes = offset % 60;

  printf("%s %s <%s> %ld %c%02d%02d\n",
       header, sig->name, sig->email, (long)sig->when.time,
       sign, hours, minutes);
}

// Printing out a blob is simple, get the contents and print
	

static void show_blob(const git_blob *blob)
{
  /* ? Does this need crlf filtering? */
  fwrite(git_blob_rawcontent(blob), (size_t)git_blob_rawsize(blob), 1, stdout);
}

// Show each entry with its type, id and attributes
static void show_tree(const git_tree *tree)
{
  size_t i, max_i = (int)git_tree_entrycount(tree);
  char oidstr[GIT_OID_HEXSZ + 1];
  const git_tree_entry *te;

  for (i = 0; i < max_i; ++i) {
    te = git_tree_entry_byindex(tree, i);

    git_oid_tostr(oidstr, sizeof(oidstr), git_tree_entry_id(te));

    printf("%06o %s %s\t%s\n",
      git_tree_entry_filemode(te),
      git_object_type2string(git_tree_entry_type(te)),
      oidstr, git_tree_entry_name(te));
  }
}


// Commits and tags have a few interesting fields in their header.
static void show_commit(git_commit *commit)
{
  unsigned int i, max_i;
  char oidstr[GIT_OID_HEXSZ + 1];

  git_oid_tostr(oidstr, sizeof(oidstr), git_commit_tree_id(commit));
  printf("tree %s\n", oidstr);

  max_i = (unsigned int)git_commit_parentcount(commit);
  for (i = 0; i < max_i; ++i) {
    git_oid_tostr(oidstr, sizeof(oidstr), git_commit_parent_id(commit, i));
    printf("parent %s\n", oidstr);
  }

  print_signature("author", git_commit_author(commit));
  print_signature("committer", git_commit_committer(commit));


  printf("\ntime: %ld\n", git_commit_time(commit));
  
  if (git_commit_message(commit))
    printf("\nMessage: %s\n", git_commit_message(commit));

  printf("\nBody: %s\n", git_commit_body(commit));
}

static void show_tag(const git_tag *tag)
{
  char oidstr[GIT_OID_HEXSZ + 1];

  git_oid_tostr(oidstr, sizeof(oidstr), git_tag_target_id(tag));;
  printf("object %s\n", oidstr);
  printf("type %s\n", git_object_type2string(git_tag_target_type(tag)));
  printf("tag %s\n", git_tag_name(tag));
  print_signature("tagger", git_tag_tagger(tag));

  if (git_tag_message(tag))
    printf("\n%s\n", git_tag_message(tag));
}

enum {
  SHOW_TYPE = 1,
  SHOW_SIZE = 2,
  SHOW_NONE = 3,
  SHOW_PRETTY = 4
};

/* Forward declarations for option-parsing helper */
struct opts {
  const char *dir;
  const char *rev;
  int action;
  int verbose;
};

static void list_tree_f (const git_tree *tree, char * pre, git_repository *repo, char * f, int depth)
{
  char oidstr1[GIT_OID_HEXSZ + 1];
  git_oid_tostr(oidstr1, sizeof(oidstr1), git_tree_id (tree));
  size_t i, max_i = (int)git_tree_entrycount(tree);
  //printf ("nentry=%ld, depth=%d %s %s\n", max_i, depth, oidstr1, pre);
  for (i = 0; i < max_i; ++i) {
      //printf ("starting=%ld\n", i);
    const git_tree_entry * te = git_tree_entry_byindex(tree, i);
    const char * name = git_tree_entry_name (te);
    char oidstr[GIT_OID_HEXSZ + 1];
     //printf ("-- %ld;%s/%s;%p\n", i, pre, name, te);
    git_oid_tostr(oidstr, sizeof(oidstr), git_tree_entry_id (te));
    
    char buf [10000];
    sprintf(buf, "%s/%s",pre, name);
    int l0 = strlen (buf);
    int l1 = strlen (f);
     //printf ("%s;%s\n", buf, f);
    if (l0 > l1 || strncmp (buf, f, l0) != 0)
       continue;
    
    if (git_tree_entry_type(te) == GIT_OBJ_BLOB) {
       //git_object * objt;
       //git_tree_entry_to_object (&objt, repo, te);
       //git_blob * gb = (git_blob*) objt;
       //int isBin = git_blob_is_binary (gb);
       //fprintf (stderr, "%s;%s;%d\n", oidstr, f, isBin);
       //if (!isBin){
       printf ("%s;%s;%s\n", oidstr, buf, f);
       //   fwrite(git_blob_rawcontent(gb), (size_t)git_blob_rawsize(gb), 1, stdout);
       //   printf ("\n%s;%s\n", oidstr, f);
       //}
       //git_object_free (objt);       
    } else {
       if (git_tree_entry_type(te) == GIT_OBJ_TREE){
          git_object * o1;
          git_tree_entry_to_object (&o1, repo, te);
          git_tree * t1 = (git_tree *) o1;
          char *result = malloc(strlen(pre)+strlen(name)+2);
          strcpy (result, pre);
          strcat (result, "/");       
          strcat (result, name);
          list_tree_f (t1, result, repo, f, depth+1);
          free (result);
          git_object_free (o1);
       } else {
	  continue;
          //fprintf (stderr, "unknown type %d for %s in %s/%s %s\n", git_tree_entry_type(te), oidstr, pre, name, f);
       }
    }
  }
}



// Entry point for this command
int main(int argc, char *argv[])
{
  git_repository *repo;
  char oidstr[GIT_OID_HEXSZ + 1];
  git_object *obj = NULL;

  git_libgit2_init();
  if (check_lg2 (git_repository_open_bare(&repo, argv[1]),
              "Could not open repository", NULL) != 0)
    exit (-1);
  char *l1 = NULL;
  size_t size = 0;
  char * pcmt = NULL;
   
   
  while (getline(&l1, &size, stdin)>=0){
    char * resL = strdup (l1);
    resL [strlen(l1)-1] = 0;
    strtok(resL, ";");
    char * l2 = strtok (NULL, ";");
    //printf ("+++ %s,%s,%s\n", l2, resL,pcmt);
    git_tree * tree;
    if (pcmt == NULL || strcmp(pcmt, resL) != 0){
      if (obj != NULL) git_object_free(obj);
      if (check_lg2(git_revparse_single(&obj, repo, resL),
		    "Could not resolve", resL) != 0){
	continue;
      }
      if (pcmt != NULL) free (pcmt);
      pcmt = strdup (resL);
    }
    strncpy (oidstr, resL, GIT_OID_HEXSZ);
    if (git_object_type (obj) == GIT_OBJ_COMMIT) {
      git_commit *commit = (git_commit*)obj;
      git_commit_tree (&tree, commit);
      //fprintf (stderr, " tree %p\n", tree); 
    } else {
      if (git_object_type (obj) == GIT_OBJ_TREE) {
	tree = (git_tree*)obj;
	//fprintf (stderr, "tree %p\n", tree); 
      } else {
	fprintf(stderr, "wrong object type: could be commit or tree\n");
	continue;
      }
    }
    list_tree_f (tree, "", repo, l2, 0);
    free (resL);
  }
  git_repository_free (repo);
  git_libgit2_shutdown ();
  
  return 0;
}



