/*
 * libgit2 "log" example - shows how to walk history and get commit info
 *
 * Written by the libgit2 contributors
 *
 * To the extent possible under law, the author(s) have dedicated all copyright
 * and related and neighboring rights to this software to the public domain
 * worldwide. This software is distributed without any warranty.
 *
 * You should have received a copy of the CC0 Public Domain Dedication along
 * with this software. If not, see
 * <http://creativecommons.org/publicdomain/zero/1.0/>.
 */

#include "common.h"
/** log_state represents walker being configured while handling options */
struct log_state {
	git_repository *repo;
	const char *repodir;
	git_revwalk *walker;
	int hide;
	int sorting;
	int revisions;
};


/** utility functions that are called to configure the walker */
static void push_rev(struct log_state *s, git_object *obj, int hide);
static int add_revision(struct log_state *s, const char *revstr);

/** utility functions that parse options and help with log output */
static int parse_options( struct log_state *s, int argc, char **argv);
static void print_time(const git_time *intime, const char *prefix);

static void print_commit(git_commit *commit);
static int match_with_parent(git_commit *commit, int i, git_diff_options *);


static void print_signature(const char *header, const git_signature *sig)
{
  char sign;
  int offset, hours, minutes;

  if (!sig)
    return;

  offset = sig->when.offset;
  if (offset < 0) {
    sign = '-';
    offset = -offset;
  } else {
    sign = '+';
  }

  hours   = offset / 60;
  minutes = offset % 60;

  printf("%s %s <%s> %ld %c%02d%02d\n",
       header, sig->name, sig->email, (long)sig->when.time,
       sign, hours, minutes);
}


// This is the output on which sha is calculated: prepending commit %n\0
static void show_commit_body (git_commit *commit)
{
  unsigned int i, max_i;
  char oidstr[GIT_OID_HEXSZ + 1];

   //git_oid_tostr(oidstr, sizeof(oidstr), git_commit_id(commit));
   //printf("commit %s\n", oidstr);
  git_oid_tostr(oidstr, sizeof(oidstr), git_commit_tree_id(commit));
  printf("tree %s\n", oidstr);

  max_i = (unsigned int)git_commit_parentcount(commit);
  for (i = 0; i < max_i; ++i) {
     git_oid_tostr(oidstr, sizeof(oidstr), git_commit_parent_id(commit, i));
     printf("parent %s\n", oidstr);
  }

  print_signature("author", git_commit_author(commit));
  print_signature("committer", git_commit_committer(commit));


   //printf("Date: %ld\n", git_commit_time(commit));
  
  if (git_commit_message(commit))
    printf("\n%s", git_commit_message(commit));

   //printf("\nBody: %s\n", git_commit_body(commit));
}

int main(int argc, char *argv[])
{
  int i, count = 0, printed = 0, parents, last_arg;
  struct log_state s;
  git_oid oid;
  git_commit *commit = NULL;
  git_pathspec *ps = NULL;

  git_libgit2_init();

  last_arg = parse_options(&s, argc, argv);
 
  if (!s.revisions)
    add_revision(&s, NULL);
  
  /** Use the revwalker to traverse the history. */
  
  printed = count = 0;
  
  for (; !git_revwalk_next(&oid, s.walker); git_commit_free(commit)) {
    check_lg2(git_commit_lookup(&commit, s.repo, &oid),
	      "Failed to look up commit", NULL);
    
    parents = (int)git_commit_parentcount(commit);
    //if (parents < 1)
    //	continue;
    print_commit(commit);
  }
  
  git_pathspec_free(ps);
  git_revwalk_free(s.walker);
  git_repository_free(s.repo);
  git_libgit2_shutdown();
  
  return 0;
}

/** Push object (for hide or show) onto revwalker. */
static void push_rev (struct log_state *s, git_object *obj, int hide)
{
  hide = s->hide ^ hide;
  
  /** Create revwalker on demand if it doesn't already exist. */
  if (!s->walker) {
    check_lg2(git_revwalk_new(&s->walker, s->repo),
	      "Could not create revision walker", NULL);
    git_revwalk_sorting(s->walker, s->sorting);
  }
  
  if (!obj)
    check_lg2(git_revwalk_push_head(s->walker),
	      "Could not find repository HEAD", NULL);
  else if (hide)
    check_lg2(git_revwalk_hide(s->walker, git_object_id(obj)),
	      "Reference does not refer to a commit", NULL);
  else
    check_lg2(git_revwalk_push(s->walker, git_object_id(obj)),
	      "Reference does not refer to a commit", NULL);
  
  git_object_free(obj);
}

/** Parse revision string and add revs to walker. */
static int add_revision (struct log_state *s, const char *revstr)
{
  git_revspec revs;
  int hide = 0;
  
  /** Open repo on demand if it isn't already open. */
  if (!s->repo) {
    if (!s->repodir) s->repodir = ".";
    check_lg2(git_repository_open_ext(&s->repo, s->repodir, 0, NULL),
	      "Could not open repository", s->repodir);
  }

  if (!revstr) {
    push_rev(s, NULL, hide);
    return 0;
  }
  
  if (*revstr == '^') {
    revs.flags = GIT_REVPARSE_SINGLE;
    hide = !hide;
    
    if (git_revparse_single(&revs.from, s->repo, revstr + 1) < 0)
      return -1;
  } else if (git_revparse(&revs, s->repo, revstr) < 0)
    return -1;
  
  if ((revs.flags & GIT_REVPARSE_SINGLE) != 0)
    push_rev(s, revs.from, hide);
  else {
    push_rev(s, revs.to, hide);
    
    if ((revs.flags & GIT_REVPARSE_MERGE_BASE) != 0) {
      git_oid base;
      check_lg2 (git_merge_base(&base, s->repo,
		 	       git_object_id(revs.from), git_object_id(revs.to)),
		"Could not find merge base", revstr);
      check_lg2 (git_object_lookup(&revs.to, s->repo, &base, GIT_OBJ_COMMIT),
		"Could not find merge base commit", NULL);
      
      push_rev(s, revs.to, hide);
    }

    push_rev(s, revs.from, !hide);
  }
  
  return 0;
}

/** Helper to print a commit object. */
static void print_commit(git_commit *commit)
{
	char buf[GIT_OID_HEXSZ + 1];
	int i, count;
	const git_signature *sig;
	const char *scan, *eol;

	git_oid_tostr(buf, sizeof(buf), git_commit_id(commit));
	printf("%s\n", buf);
}

/** Parse some log command line options. */
static int parse_options(
			 struct log_state *s, int argc, char **argv)
{
  struct args_info args = ARGS_INFO_INIT;

  memset (s, 0, sizeof(*s));
  s->sorting = GIT_SORT_TIME;


  for (args.pos = 1; args.pos < argc; ++args.pos) {
    const char *a = argv[args.pos];
    
    if (a[0] != '-') {
      if (!add_revision(s, a))
	s->revisions++;
      else
	/** Try failed revision parse as filename. */
	break;
    } else if (!strcmp(a, "--")) {
      ++args.pos;
      break;
    }
    if (match_str_arg(&s->repodir, &args, "--git-dir"))
      /** Found git-dir. */;
  }
  return args.pos;
}

